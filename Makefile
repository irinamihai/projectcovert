CC=gcc

M1=10.0.3.48
M2=10.0.3.116
p1=root
p2=root

make:
	$(CC) -Wno-overflow sender.c common.c crc32.c -o sender
	$(CC) -Wno-overflow receiver.c common.c crc32.c -o receiver
	M1=$(M1) p1=$(p1) M2=$(M2) p2=$(p2) ./deploy.sh
clean:
	rm -f sender
	rm -f receiver
