#include<stdio.h>
#include<stdlib.h>
#include<sys/time.h>
#include<stdint.h>

double MAX_EXECUTED_OPS_EVEEER = 0;

#define DEBUG_PRINT

inline uint64_t rdtsc()
{
    uint32_t lo, hi;
    __asm__ __volatile__ (
      "xorl %%eax, %%eax\n"
      "cpuid\n"
      "rdtsc\n"
      : "=a" (lo), "=d" (hi)
      :
      : "%ebx", "%ecx" );
    return (uint64_t)hi << 32 | lo;
}

// 4MB memory
#define MEMORY_SIZE 4*1024*1024
int thrash_cache()
{
  int i;
  static int result = 0;
  static char* memory = 0;
  int next_second;
  struct timeval systime;
  gettimeofday(&systime, 0);
  next_second = systime.tv_sec + 1;

  if(!memory) memory = (char*)malloc(MEMORY_SIZE);

  do {
    for(i = 0; i<MEMORY_SIZE/2; i+=256)
    {
      // better thrashing the cache?
      result += memory[i];
      result += memory[MEMORY_SIZE/2 + i];
    }
    gettimeofday(&systime, 0);
  } while (systime.tv_sec < next_second);

#ifdef DEBUG_PRINT
  printf("Thrashed the cache, exited at: %lld,%lld\n", (long long)systime.tv_sec, (long long)systime.tv_usec);
#endif

  return result;
}

int cycles[4096];

/* Get number of operations until next full second. */
double get_ops_until_next_second()
{
#define CYCLES_PER_OP 1
  uint64_t sum = 0;
  int j;
  int next_second;
  uint64_t sss = 0;
  int loops = 0;
  struct timeval systime;
  gettimeofday(&systime, 0);
  next_second = systime.tv_sec + 1;
  
 // cycles = 0;
  do {
 //   for(j = 0; j<CYCLES_PER_OP; j++)
    {
      uint64_t t1, t2;
      t1 = rdtsc();
      cycles[1024]++;
      t2 = rdtsc();
      sum += (t2-t1);
      usleep(10000);
    }
    gettimeofday(&systime, 0);
    loops += CYCLES_PER_OP;
  } while (systime.tv_sec < next_second);

  //cycles /= CYCLES_PER_OP;

//  if(sum/loops > MAX_EXECUTED_OPS_EVEEER)
//    MAX_EXECUTED_OPS_EVEEER = sum/loops;

#ifdef DEBUG_PRINT
  printf("OPS = %llu, MAX = %.0lf\n", sum/loops, MAX_EXECUTED_OPS_EVEEER);
#endif

  return 10;//cycles[1024];
}

/* Get number of operations until next count seconds. */
void get_ops_for_seconds(int count) {
  while(count --)
    get_ops_until_next_second();
}

/* count = 1 will wait until the next integer second. */
void wait_seconds(int count)
{
  struct timeval systime1, systime2;
  int useconds_left;
  gettimeofday(&systime1, 0);
  useconds_left = 1000000 * count - systime1.tv_usec;
  usleep(useconds_left);
#ifdef DEBUG_PRINT
  gettimeofday(&systime2, 0);
  printf("Slept: %lld,%lld  --  %lld,%lld\n", (long long)systime1.tv_sec, (long long)systime1.tv_usec, (long long)systime2.tv_sec, (long long)systime2.tv_usec);
#endif
}

void sendBit(int B)
{
  /* total silence == sending bit 0 */
  /* high cpu usage == sending bit 1 */
  if((B & 0x1) == 0)
    wait_seconds(1);
  else
    thrash_cache();
}

void sendBits(int B, int count)
{
  /* MSB order */
  while(count--)
  {
    sendBit(B >> count);
  }
}

int receiveBit()
{
#define RECV_THRESHOLD 0.7

  double ops = get_ops_until_next_second();
  if(ops > MAX_EXECUTED_OPS_EVEEER * RECV_THRESHOLD)
    return 0;
  else
    return 1;
}

int receiveBits(int count, int result)
{
  /* MSB order */
  while(count--)
  {
    result = (result<<1) | receiveBit();
  }

  return result;
}
