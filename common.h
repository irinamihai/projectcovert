/* Defines. */
#define SHUT_UP            0x6B
#define SHUT_UP_TIME       4
#define ACK                0x17
#define HEADER             0xDA
#define BYTE_SIZE          8
/* End defines. */

/* Functions. */
void wait_seconds(int count);

void sendBit(int B);
void sendBits(int B, int count);

int receiveBit();
int receiveBits(int count, int result);

/*End functions. */
