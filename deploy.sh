#!/bin/bash

if [[ -f `which sshpass` ]]; then
sshpass -p "${p1}" scp -o StrictHostKeyChecking=no sender root@${M1}:/
sshpass -p "${p2}" scp -o StrictHostKeyChecking=no receiver root@${M2}:/
sshpass -p "${p1}" ssh -o StrictHostKeyChecking=no root@${M1} "echo 'Yoda lives!' > /file"
else
scp -o StrictHostKeyChecking=no sender root@${M1}:/
scp -o StrictHostKeyChecking=no receiver root@${M2}:/
ssh -o StrictHostKeyChecking=no root@${M1} "echo 'Yoda lives!' > /file"
fi

echo 'Apps deployed'
