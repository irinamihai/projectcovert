#include<stdio.h>
#include<unistd.h>
#include"common.h"

//#define DEBUG_PRINT        1
FILE *f;

/* Listen for the specific CPU load pattern that indicates a communication 
   request. */
void listen_for_sync_pattern() {
  int count = 0;
  int bits = 0;
  
  while((bits & SHUT_UP) != SHUT_UP) {
    bits = receiveBits(1, bits);
#ifdef DEBUG_PRINT
    printf("Got %X\n", bits);
#endif
  }
  
#ifdef DEBUG_PRINT
  if(bits == SHUT_UP)
    printf("Send ACK!!\n");
#endif
 
  /* Send ACK for the shutting up message. */
  sendBits(ACK, BYTE_SIZE);

#ifdef DEBUG_PRINT
  printf("ACK sent, sleep!!\n");
#endif

  /* Sleep for giving time to the sender to calibrate itself. */
  wait_seconds(SHUT_UP_TIME);
}

/* Translate received payload into ASCII characters. */
void translate_into_ascii(int received) {
  int i;

  for(i=3; i>=0; i--) {
    char c = (received >> (BYTE_SIZE*i)) & 0xFF;
#ifdef DEBUG_PRINT
    printf("Character: %c\n", c);
#endif
    fprintf(f, "%c", c);
  }
}

/* Listen for incoming packets. */
void start_listening() {
  int received;
  int id;
  int previous_id = -1;
  int i;
  int crc, crc_received;

  do {
#ifdef DEBUG_PRINT
    printf("Getting payload.\n");
#endif
    received = receiveBits(BYTE_SIZE*4, 0);
#ifdef DEBUG_PRINT
    printf("Got %X\n", received);
#endif

#ifdef DEBUG_PRINT
    printf("\nGetting ID:\n");
#endif
    id = receiveBits(BYTE_SIZE, 0);
#ifdef DEBUG_PRINT
    printf("Got %X\n", id);
#endif

#ifdef DEBUG_PRINT
    printf("\nGetting CRC:\n");
#endif
    crc_received = receiveBits(BYTE_SIZE, 0);
#ifdef DEBUG_PRINT
    printf("Got %X\n", crc_received);
#endif

    crc = crc32(0L, NULL, 0);

    crc = crc32(crc, (char *)&received, 4);
    crc = crc32(crc, (char *)&id, 1);
    crc = crc & 0xFF;

    if((crc_received == 0xFF) && (received == 0xFFFFFFFF)) {
      printf("Got message to stop\n");
      fclose(f);
      return;
    }

    if(crc == crc_received) {
#ifdef DEBUG_PRINT
      printf("\n!!! CRC OK  !!!\n");
#endif

     printf("Received package ok\n");    

      sendBits(ACK, BYTE_SIZE);

      /* If this is a packet that has been sent before, don't translate it.
         This can happen if the ACK sent to the sender is lost. */
      if(id != previous_id) {
        translate_into_ascii(received);
        previous_id = id;
      }
    }
    else {
#ifdef DEBUG_PRINT
      printf("\n!!! CRC MISMATCH !!!\n");
#endif
      printf("Received package malformed\n");

      sendBits(0, BYTE_SIZE);
    }
#ifdef DEBUG_PRINT
  printf("Got %X with id %d, crc %X. Computed crc %X\n", received, id, crc_received, crc);
#endif
  } while(1);
}

int main() {

  listen_for_sync_pattern();

  f = fopen("file", "w+");

  printf("Sync ok, starting listening...\n");

  start_listening();
}
