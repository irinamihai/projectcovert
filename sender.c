#include<stdio.h>
#include<unistd.h>
#include"common.h"
#include"crc32.h"

//#define DEBUG_PRINT        1

/* Create the specific CPU load pattern that indicates a communication 
   request. */
void send_sync_pattern() {
  int shut_up[8] = {0, 1, 1, 0, 1, 0, 1, 1};
  int V = 0x6B;
  int ACK_RCV = 0;
  int i;

  /* Give it time to the receiver to calibrate. */
  wait_seconds(2);
  
  do {
    /* Send shut up pattern to the receiver to give time for calibration. */
    sendBits(V, BYTE_SIZE);

    /* Check that the receiver has understood to shut up. */
    for(i=0; i<BYTE_SIZE+4; i++) { 
      ACK_RCV = receiveBits(1, ACK_RCV);
#ifdef DEBUG_PRINT
      printf("Got %X\n", ACK_RCV);
#endif

      if((ACK_RCV & ACK) == ACK)
        break;
    }

  } while ((ACK_RCV & ACK) != ACK);

#ifdef DEBUG_PRINT
  printf("Calibrate now.\n");
#endif

  /* Calibrate MAX_EXECUTED_OPS_EVEEER. */
  receiveBits(SHUT_UP_TIME, 0);

#ifdef DEBUG_PRINT
  printf("Done! Start transmission!\n");
#endif
}

/* Send packet: <id><payload><CRC>. */
void send_packet(int payload, int id) {
  unsigned int crc = crc32(0L, NULL, 0);
  int i; 
  int ACK_RCV;

  printf("Sending package %d\n", id);

  /* Compute CRC. */
  crc = crc32(crc, (char *)&payload, 4);
  crc = crc32(crc, (char *)&id, 1);
  crc = crc & 0xFF;

#ifdef DEBUG_PRINT
  printf("crc: %X\n", crc);
#endif

  do {
    /* Send payload. */
    sendBits(payload, BYTE_SIZE*4);

    /* Send id. */
    sendBits(id, BYTE_SIZE);

    /* Send CRC. */
    sendBits(crc, BYTE_SIZE);

    /* Wait for ACK. */
    ACK_RCV = receiveBits(BYTE_SIZE, 0);

#ifdef DEBUG_PRINT
    printf("Got ACK, %X!\n", ACK_RCV);
#endif
  } while (ACK_RCV != ACK);
}

/* Send the packet for ending the communication. */
void send_stop_packet(int id) {
  /* Send payload. */
  sendBits(0xFFFFFFFF, BYTE_SIZE*4);

  /* Send id. */
  sendBits(id, BYTE_SIZE);

  /* Send CRC. */
  sendBits(0xFF, BYTE_SIZE);
}

/* Read input file and send four characters at a time. */
int read_file() {
  FILE *f = fopen("file", "rb+");
  int number = 0;
  int i = 3;
  int id = 0;
  int c;

  if(f == NULL) {
    printf("ERROR at opening file for reading!");
    return 1;
  }

  /* Form an integer containing 4 characters. */
  while(EOF != (c = fgetc(f))) {
    number = number | (c << (i*BYTE_SIZE)); 

    i--;

    if(i == -1) {
#ifdef DEBUG_PRINT
      printf("Sending %X\n", number);
#endif
      send_packet(number, id);
      id++;
      i = 3;
      number = 0;
    }
  }

  /* If there are still bytes on the queue, send them. */
  if(i != 3) {
#ifdef DEBUG_PRINT
    printf("Sending %X\n", number);
#endif
    send_packet(number, id);
  }

  /* Send message that transmission will stop. */
  send_stop_packet(id+1);

  fclose(f);

  return 0;
}

int main() {
  send_sync_pattern();

  read_file();
}
